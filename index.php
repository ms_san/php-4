<?php

require 'animal.php';
require 'ape.php';
require 'frog.php';

$sheep = new Animal("shaun");
echo $sheep->name; 
echo "<br>";
echo $sheep->legs; 
echo "<br>";
echo var_export($sheep->cold_blooded);
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
$sungokong->yell();

echo "<br>";
echo "<br>";
$kodok = new Frog("buduk");
$kodok->jump() ;

?>